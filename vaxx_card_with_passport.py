import hes_api
import time


def parse_passport_list(passport_list: dict) -> dict:
    return {
        passport["passportNumber"]: passport["userPassportId"]
        for passport in passport_list
    }


def prompt_add_passport(hes: hes_api.HESAPI, passport_number: str = None) -> str:
    if not passport_number:
        passport_number = input(
            f"Please enter the passport number you want to use for your vaccine card (example: U12345678): "
        )
    country_code = input(
        "Please enter the passport issuer's 3 character country code (example: TUR - Please use D for Germany): "
    )
    last_name = input("Please enter your last name: ")
    year_of_birth = input("Please enter your year of birth (example: 1995): ")
    hes.add_passport(passport_number, country_code, last_name, year_of_birth)
    return passport_number


def manual_logic():
    hes = hes_api.HESAPI()
    phone_number = input(
        "Please enter your phone number with country code (example: +905321234567): "
    )
    hes.send_login_code(phone_number)
    account_info = hes.authenticate_with_code(
        phone_number,
        input("Please enter the code you just received (example: 123456): "),
    )
    if not account_info["mernisChecked"]:
        # yes this sucks but eh
        print("You haven't registered to HES yet, so I'll handle that now.")
        print("Please note that this only works for Turkish citizens with ID cards.")
        print("Use the app to register if this qualification does not apply to you.")

        identity_number = input(
            "Please enter your ID number (aka TCKN, example: 11111111111): "
        )
        year_of_birth = input("Please enter your year of birth (example: 1995): ")
        doc_num_last_four = input(
            "Please enter the last 4 characters of your new ID card's document number (Example for a document number of A12Z34567: 4567): "
        )
        name_of_father = input("First name of your father: ")

        hes.register_account(
            identity_number, year_of_birth, doc_num_last_four, name_of_father
        )
        account_info = hes.get_account_info()

    vacc_status = hes.get_vaccination_and_card_status()
    if len(vacc_status["availableVaccineCardList"]) == 0:
        print("You do not qualify for any vaccine cards at this time. Exiting.")
        return

    passport_list = hes.get_passports()
    passport_details = parse_passport_list(passport_list)

    if not passport_list:
        print("You do not have any passports on HES.")
        passport_number = prompt_add_passport(hes)

        # Refresh list after adding passport
        passport_list = hes.get_passports()
        passport_details = parse_passport_list(passport_list)
    else:
        print(f"Your passports on HES: {', '.join(list(passport_details.keys()))}")

        print(
            f"Please enter the passport number you want to use for your vaccine card (example: {list(passport_details.keys())[0]})"
        )
        passport_number = input(
            f"(If your passport number is not already added, you will be given the ability to add it): "
        ).strip()
        if passport_number not in passport_details.keys():
            passport_number = prompt_add_passport(hes, passport_number)

            # Refresh list after adding passport
            passport_list = hes.get_passports()
            passport_details = parse_passport_list(passport_list)

    # TODO: Maybe let's not nuke everything beforehand?
    vaccine_cards = hes.get_vaccine_cards()["sonuc"]
    for vaccine_card in vaccine_cards:
        hes.delete_vaccine_card(vaccine_card["uniqueKod"])

    print("Generating vaccine card...")
    hes.create_vaccine_card(
        vacc_status["availableVaccineCardList"][0]["id"],
        passport_details[passport_number],
    )
    vaccine_cards = hes.get_vaccine_cards()["sonuc"]
    while not vaccine_cards or vaccine_cards[0]["durum"] == 0:
        time.sleep(1)
        vaccine_cards = hes.get_vaccine_cards()["sonuc"]

    save_filename = vaccine_cards[0]["uniqueKod"] + ".pdf"
    print(f"Saving vaccine card to {save_filename}.")
    with open(save_filename, "wb") as f:
        f.write(hes.get_vaccine_card_pdf(vaccine_cards[0]["pdfPath"]))


if __name__ == "__main__":
    manual_logic()
