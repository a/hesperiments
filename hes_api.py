import requests


class HESAPI:
    def __init__(self, token: str = None):
        self.sess = requests.Session()
        self.sess.headers.update(
            {
                "ver": "2.3.5",
                "platform": "2",
                "Authorization": f"Bearer {token}" if token else "Bearer",  # yea
                "User-Agent": "Hayat Eve Sigar/2.3.5 (tr.gov.saglik.hayatevesigar; build:1; iOS 13.4.1) Alamofire/5.1.0",
            }
        )

    def send_login_code(self, phone_number: str):
        req = self.sess.post(
            "https://hessvc.saglik.gov.tr/api/send-code-to-login",
            json={"phone": phone_number},
        )
        assert req.status_code == 201

    def authenticate_with_code(self, phone_number: str, sms_code: str) -> dict:
        req = self.sess.post(
            "https://hessvc.saglik.gov.tr/api/authenticate-with-code",
            json={"phone": phone_number, "rememberMe": True, "password": str(sms_code)},
        )
        assert req.status_code == 200
        reqj = req.json()
        self.sess.headers.update({"Authorization": f"Bearer {reqj['id_token']}"})
        return reqj

    def register_account(
        self,
        identity_number: int,
        year_of_birth: int,
        doc_num_last_four: int,
        name_of_father: str,
    ) -> dict:
        req = self.sess.post(
            "https://hessvc.saglik.gov.tr/api/register-with-mernis",
            json={
                "birthYear": int(year_of_birth),
                "serialNumberLast4Digit": str(doc_num_last_four),
                "fatherName": str(name_of_father),
                "tc": str(identity_number),
            },
        )
        assert req.status_code == 200
        return req.json()

    def get_account_info(self) -> dict:
        req = self.sess.get("https://hessvc.saglik.gov.tr/api/account-with-token")
        assert req.status_code == 200
        return req.json()

    def get_vaccination_and_card_status(self) -> dict:
        req = self.sess.get(
            "https://hessvc.saglik.gov.tr/api/vaccine-and-card-status-v2"
        )
        assert req.status_code == 200
        return req.json()

    def get_vaccine_cards(self) -> dict:
        req = self.sess.get("https://hessvc.saglik.gov.tr/api/vaccine-card/list")
        assert req.status_code == 200
        return req.json()

    def create_vaccine_card(self, vaccine_type: int, passport_id: str = None) -> dict:
        vaccine_card_data = {
            "cardName": "",
            "vaccineType": int(vaccine_type),
            "identityType": "IDENTITY_CARD",
        }

        if passport_id:
            vaccine_card_data["userPassportId"] = passport_id

        req = self.sess.post(
            "https://hessvc.saglik.gov.tr/api/vaccine-card/add", json=vaccine_card_data
        )
        assert req.status_code == 200
        return req.json()

    def delete_vaccine_card(self, vaccine_card_id: str) -> dict:
        req = self.sess.delete(
            f"https://hessvc.saglik.gov.tr/api/vaccine-card/delete/{vaccine_card_id}"
        )
        assert req.status_code == 200
        return req.json()

    def get_vaccine_card_pdf(self, vaccine_card_pdf_path: str) -> bytes:
        req = self.sess.get(
            f"https://hessvc.saglik.gov.tr/api/vaccine-card/download?fileName={vaccine_card_pdf_path}",
        )
        assert req.status_code == 200
        return req.content

    def get_passports(self) -> dict:
        req = self.sess.get("https://hessvc.saglik.gov.tr/api/user-passport")
        assert req.status_code == 200
        return req.json()

    def delete_passport(self, passport_id: str):
        req = self.sess.delete(
            f"https://hessvc.saglik.gov.tr/api/user-passport/remove/{passport_id}"
        )
        assert req.status_code == 200
        # Yes, no response, but also not 204.

    def add_passport(
        self,
        passport_number: str,
        country_code: str,
        last_name: str,
        year_of_birth: int,
    ):
        req = self.sess.post(
            f"https://hessvc.saglik.gov.tr/api/user-passport/add",
            json={
                "countryCode": str(country_code),
                "birthYear": int(year_of_birth),
                "identityValidationSource": "MANUEL",
                "lastName": str(last_name.upper()),
                "passportNumber": str(passport_number),
            },
        )
        assert req.status_code == 200
        # Yes, no response, but also not 201 or 204.

    def check_hes_code(self, hes_code: str):
        req = self.sess.post(
            f"https://hessvc.saglik.gov.tr/services/hescodeproxy/api/check-hes-code-plus",
            json={"hes_code": hes_code},
        )
        assert req.status_code == 200
        return req.json()
