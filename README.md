# hesperiments

Experimental interoperability project to to be able to perform certain tasks from non-iOS/Android platforms, based on examinations of Hayat Eve Sigar / HES (Turkey's COVID-19 app).

## Requirements

- Install python3, a modern version (~>3.7)
- Install dependencies (`pip3 install -Ur requirements.txt`)

## Components

### vaxx_card_with_passport

vaxx_card_with_passport is a simple script to generate a vaccination card with a passport number on it. This is important for those wishing to travel with their vaccination card, and this type of vaccination card is currently not possible to generate on e-nabiz:

![](https://pbs.twimg.com/media/E6l-W9rXsAkLUAl?format=jpg&name=orig)

Simply run this script with `python3 vaxx_card_with_passport.py` and it will guide you through the process of generating one:

![](https://cdn.discordapp.com/attachments/688735163882012704/866389270575054849/unknown.png)

(Please note that the code is not the cleanest at this time.)

## Legal note and disclaimers

This interoperability project was developed pursuant to Clause 38 of Turkish Fikir Sanat Kanunu (law nr 5846), and some information in it has been obtained through observing and examining the Hayat Eve Sığar app ("HES") released for iOS operating system.

The developers of this project do not accept any responsibility for any problems that may arise from use of this project.

## Yasal notlar ve sorumluluk reddi

5846 nolu Fikir Sanat Kanunu'nun Madde 38'i uyarınca araişlerlik amacıyla geliştirilen bu projedeki bazı bilgiler iOS işletim sistemi için olan Hayat Eve Sığar uygulamasının ("HES") işlenişi gözlenerek ve tetkik edilerek edinilmiştir.

Bu projenin geliştiricileri bu projenin kullanımı sonucu oluşabilecek herhangi bir sorunla alakalı sorumluluk kabul etmemektedir.
